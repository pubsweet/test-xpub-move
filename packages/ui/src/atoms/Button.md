A standard button.

```js
<Button>Save</Button>
```

A standard button can be disabled.

```js
<Button disabled>Save</Button>
```

A standard button can be marked as the "primary" action.

```js
<Button primary>Save</Button>
```

A button can also be "plain" style.

```js
<Button plain>Plain button</Button>
```

Or be "plain" style and "disabled".

```js
<Button plain disabled>
  Plain button
</Button>
```
