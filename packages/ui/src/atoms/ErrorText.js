import styled from 'styled-components'
import th from '../helpers/themeHelper'

const ErrorText = styled.div`
  color: ${th('colorError')};
`

/**
 * @component
 */
export default ErrorText
