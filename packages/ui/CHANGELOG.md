# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="3.0.0"></a>
# [3.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui@2.1.1...@pubsweet/ui@3.0.0) (2018-03-05)


### Bug Fixes

* update snapshot tests to use theming ([8ffd0e7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8ffd0e7))
* **ui:** color and font display in styleguide ([d20affd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d20affd))
* **ui:** eslint error ([080db31](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/080db31))
* **ui:** regularise color and spacing of ValidatedField ([4e6ce57](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4e6ce57))
* **ui:** tests for YesOrNo ([2ba7d6a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2ba7d6a))
* **ui:** update snapshot ([827406e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/827406e))
* **ui:** update snapshots ([79fca90](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/79fca90))
* **ui:** update snapshots ([616ca7e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/616ca7e))
* **ui:** variable names in colors.md and fonts.md ([6347b04](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6347b04))


### Code Refactoring

* **ui:** refactor file and files components ([8e76691](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8e76691))
* **ui:** tidy AppBar ([09751b6](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/09751b6))
* **ui:** wrap Icon with Colorize ([68ad6cd](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/68ad6cd))


### Features

* **default-theme:** add variables to default theme ([ba121b0](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ba121b0))
* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **ui:** add theming to Attachments ([8324704](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/8324704))
* **ui:** add theming to Radio ([d97596f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d97596f))
* **ui:** add theming to StateItem ([b5868d5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/b5868d5))
* **ui:** add theming to Tags ([ee959d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee959d2))
* **ui:** add theming to UploadingFile ([c589f4f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c589f4f))
* **ui:** add theming to ValidatedField ([c2a1d54](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c2a1d54))


### BREAKING CHANGES

* **ui:**  * Icon takes semantic color props instead of a color name
* **ui:**   * navLinks prop is now navLinkComponents and expects an array of
elements
* **ui:** * `Files` (renamed to `FileUploadList`) takes a single component that will receive `uploaded` prop
when upload is complete
* `Attachment` has default and uploaded state
* `UploadingFile` has default and uploaded state (`File` is deprecated)
* `UploadingFile`, `File`, `Files`, `Supplementary`, and `Attachments` takes `files` prop instead of `values`
* `Icon` size prop is now a multiplier for sub-grid-unit

Other changes:
* Theming and grid-spacing applied to all touched components




<a name="2.1.1"></a>
## [2.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui@2.1.0...@pubsweet/ui@2.1.1) (2018-02-23)


### Bug Fixes

* **ui:** add spinner in index.js ([84ecec1](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/84ecec1))




<a name="2.1.0"></a>

# [2.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui@2.0.0...@pubsweet/ui@2.1.0) (2018-02-16)

### Features

* **component:** add file picker component ([4fcb74f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/4fcb74f))
* **component:** add progress steps component ([e4b77c4](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e4b77c4))
* **component:** add spinner component ([3a4ae9a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3a4ae9a))

<a name="2.0.0"></a>

# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui@1.0.0...@pubsweet/ui@2.0.0) (2018-02-08)

### Bug Fixes

* **ui:** fix bug in Menu (not yet converted to styled component) ([5183438](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5183438))
* **ui:** fixes to Radio, pre-conversion ([7b9239b](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7b9239b))

### Features

* **ui:** convert AlignmentBox to a styled component ([7241128](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/7241128))
* **ui:** convert AlignmentBoxWithLabel, AlignmentTool to styled-comp ([ecb50ff](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ecb50ff))
* **ui:** convert AppBar to a styled component ([6527724](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6527724))
* **ui:** convert Attachment to a styled component ([0cd5f39](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/0cd5f39))
* **ui:** convert Attachments to a styled component ([f90d98c](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f90d98c))
* **ui:** convert Avatar to a styled component ([cc97cba](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/cc97cba))
* **ui:** convert Badge to a styled component ([ec720c2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ec720c2))
* **ui:** convert Button to a styled component ([9c84dc9](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9c84dc9))
* **ui:** convert Checkbox to a styled component ([3cebeec](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/3cebeec))
* **ui:** convert File to a styled component ([bb339ca](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bb339ca))
* **ui:** convert Files to a styled component ([c8a3b39](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/c8a3b39))
* **ui:** convert Icon to a styled component ([f6afa82](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/f6afa82))
* **ui:** convert Menu to a styled component ([411d3e3](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/411d3e3))
* **ui:** convert Radio to styled component ([6928c31](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6928c31))
* **ui:** convert StateItem to styled component ([90b882f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/90b882f))
* **ui:** convert StateList to a styled component ([00d800a](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/00d800a))
* **ui:** convert Tags to a styled component ([ca4b180](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ca4b180))
* **ui:** convert TextField to a styled component ([31066fb](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/31066fb))
* **ui:** convert UploadingFile to a styled component ([9bc81f7](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9bc81f7))
* **ui:** give unselected menu items a different colour ([087013f](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/087013f))
* **ui:** remove name prop and refactor ([32d5d69](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/32d5d69))
* **ui:** use styled-components 2 for jest-styled-components compat ([ca281cc](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ca281cc))

### BREAKING CHANGES

* **ui:** StateItem doesn't accept the 'name' prop anymore, as it wasn't used.

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/ui@0.2.3...@pubsweet/ui@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
