# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="2.2.0"></a>
# [2.2.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.1.1...pubsweet-client@2.2.0) (2018-03-05)


### Bug Fixes

* downgrade styled-components dependency in pubsweet-client ([718558e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/718558e))
* update Root to use new StyleRoot component ([9d4c0ef](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9d4c0ef))


### Features

* **normalize:** add normalize css ([9eb24e5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/9eb24e5))
* **ui:** add theming to Tags ([ee959d2](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/ee959d2))




<a name="2.1.1"></a>

## [2.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.1.0...pubsweet-client@2.1.1) (2018-02-16)

### Bug Fixes

* **client:** remove unused dependency on login component ([6c5dd97](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/6c5dd97))

<a name="2.1.0"></a>

# [2.1.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@2.0.0...pubsweet-client@2.1.0) (2018-02-08)

### Features

* **client:** add styled components ([43ab2c5](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/43ab2c5))

<a name="2.0.0"></a>

# [2.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-client@1.1.4...pubsweet-client@2.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16
