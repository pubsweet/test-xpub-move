# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.0"></a>
# 0.1.0 (2018-03-05)


### Bug Fixes

* **elife-theme:** switch to JS theme object ([947e29d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/947e29d))


### Features

* **elife-theme:** add elife theme ([e406e0d](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/e406e0d))




<a name="0.1.0"></a>

# 0.1.0 (2018-02-08)

### Features

* **theme:** add default theme package ([5231a56](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/5231a56))
