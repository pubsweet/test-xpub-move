import { history } from 'prosemirror-history'

import keys from './keys'

export default [keys, history()]
