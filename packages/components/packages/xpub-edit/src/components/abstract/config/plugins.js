import { history } from 'prosemirror-history'
import { dropCursor } from 'prosemirror-dropcursor'
import { gapCursor } from 'prosemirror-gapcursor'
import keys from './keys'

export default [keys, dropCursor(), gapCursor(), history()]
