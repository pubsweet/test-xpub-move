## xpub-edit (pubsweet-edit)  

A configurable editor built with ProseMirror, allowing rich-text form inputs to have a defined schema and custom toolbars.

*Note:  
This will be moved to pubsweet.*
