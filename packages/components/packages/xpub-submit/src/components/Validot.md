A dot representing the validation state of a form field.

### Idle

```js
;<Validot />
```

### Warning

```js
;<Validot warning message="There was a warning" />
```

### Error

```js
;<Validot error message="There was an error" />
```

### Valid

```js
;<Validot valid />
```
