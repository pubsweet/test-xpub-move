module.exports = {
  title: 'xpub submit style guide',
  styleguideComponents: {
    StyleGuideRenderer: require.resolve(
      'xpub-styleguide/src/components/StyleGuideRenderer',
    ),
    Wrapper: require.resolve('xpub-styleguide/src/components/Wrapper'),
  },
  context: {
    faker: 'faker',
  },
  skipComponentsWithoutExample: true,
  serverPort: 6064,
  theme: {
    fontFamily: {
      base: '"Fira Sans", sans-serif',
    },
    color: {
      link: 'cornflowerblue',
    },
  },
}
