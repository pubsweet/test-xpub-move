# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.1"></a>

## [0.1.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/@pubsweet/component-aws-s3@0.1.0...@pubsweet/component-aws-s3@0.1.1) (2018-02-16)

**Note:** Version bump only for package @pubsweet/component-aws-s3

<a name="0.1.0"></a>

# 0.1.0 (2018-02-08)

### Features

* **components:** added aws s3 ([73c0764](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/73c0764))
