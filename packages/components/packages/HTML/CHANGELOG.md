# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.6"></a>
## [0.2.6](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.5...pubsweet-component-html@0.2.6) (2018-03-05)


### Bug Fixes

* **components:** make styleguide work (mostly) ([d036681](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/d036681))




<a name="0.2.5"></a>

## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-html@0.2.4...pubsweet-component-html@0.2.5) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-html
