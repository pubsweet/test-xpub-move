## xpub-connect (pubsweet-connect)

This allows components to be defined as part of the composition of a functional component (as an alternative to handling data dependencies in the react lifecycle).
