# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.0.3"></a>
## [1.0.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.2...pubsweet-component-posts-manager@1.0.3) (2018-03-05)


### Bug Fixes

* **components:** correctly redirect when edit button is clicked ([faca509](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/faca509))




<a name="1.0.2"></a>
## [1.0.2](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.1...pubsweet-component-posts-manager@1.0.2) (2018-02-23)




**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.1"></a>

## [1.0.1](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@1.0.0...pubsweet-component-posts-manager@1.0.1) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-posts-manager

<a name="1.0.0"></a>

# [1.0.0](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@0.6.10...pubsweet-component-posts-manager@1.0.0) (2018-02-02)

### Features

* **client:** upgrade React to version 16 ([626cf59](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/626cf59)), closes [#65](https://gitlab.coko.foundation/pubsweet/pubsweet/issues/65)

### BREAKING CHANGES

* **client:** Upgrade React to version 16

<a name="0.6.10"></a>

## [0.6.10](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-posts-manager@0.6.9...pubsweet-component-posts-manager@0.6.10) (2018-02-02)

**Note:** Version bump only for package pubsweet-component-posts-manager
