# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.3"></a>

## [0.2.3](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-manage@0.2.2...pubsweet-component-manage@0.2.3) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-manage
