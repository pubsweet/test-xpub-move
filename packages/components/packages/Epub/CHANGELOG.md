# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.12"></a>

## [0.2.12](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-epub@0.2.11...pubsweet-component-epub@0.2.12) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-epub
