module.exports = {
  'pubsweet-component-aws-ses': {
    secretAccessKey: 'secret-key',
    accessKeyId: 'access-key',
    region: 'region',
    sender: 'test_sender@domain.com',
  },
}
