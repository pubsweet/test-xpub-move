# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.0"></a>

# 0.1.0 (2018-02-16)

### Bug Fixes

* **component:** fix tests ([bf9d13e](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/bf9d13e))

### Features

* **component:** add aws ses package ([2e34627](https://gitlab.coko.foundation/pubsweet/pubsweet/commit/2e34627))
