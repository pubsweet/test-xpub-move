import styled from 'styled-components'

const Actions = styled.div``

const ActionContainer = styled.div`
  display: inline-block;
`

export { Actions, ActionContainer }
