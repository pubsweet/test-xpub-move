## xpub-journal

`xpub-collabra` is built for a single journal. As there was no place for a journal in the data model, the journal configuration is passed into the app via this JournalProvider component. 

*Note:  
This will probably need to be replaced by journal configuration in the database, in order to support multiple journals in xpub apps.  
To be revisited after we make a more final decision on xpub's data model.*
