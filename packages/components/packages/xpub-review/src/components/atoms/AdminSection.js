import styled from 'styled-components'
import { th } from '@pubsweet/ui'

const AdminSection = styled.div`
  margin-bottom: ${th('gridUnit')};
`

export default AdminSection
