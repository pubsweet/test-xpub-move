# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.2.5"></a>

## [0.2.5](https://gitlab.coko.foundation/pubsweet/pubsweet/compare/pubsweet-component-ink-backend@0.2.4...pubsweet-component-ink-backend@0.2.5) (2018-02-16)

**Note:** Version bump only for package pubsweet-component-ink-backend
