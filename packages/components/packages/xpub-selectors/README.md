## xpub-selectors  

Helper functions for common selectors used in redux compose, so that components don't need to know the unique shape of each part of the store.  

*Note:  
Some of these selectors will probably move to `pubsweet-client`.*