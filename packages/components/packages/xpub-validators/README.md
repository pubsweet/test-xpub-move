## xpub-validators

Common validator functions for form fields, for use with `redux-form`.

*Note  
Likely generic enough to be moved to the pubsweet repo.*
