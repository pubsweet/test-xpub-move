/* eslint-disable import/extensions */
import 'typeface-noto-sans'
import 'typeface-noto-serif'
import 'typeface-ubuntu-mono'

export default {
  /* Colors */
  colorBackground: 'white',
  colorPrimary: '#205d86',
  colorSecondary: '#e7e7e7',
  colorQuiet: '#aaa',
  colorFurniture: '#ccc',
  colorBorder: '#aaa',
  colorBackgroundHue:
    '#f1f1f1' /* marginally darker shade of the app background so that it can be used to divide the interface when needed */,
  colorSuccess: '#050',
  colorError: '#b50000',
  colorWarning: '#ffcc00',
  colorText: '#333',
  colorTextReverse: '#fff',
  colorTextPlaceholder: '#595959',

  /* Text variables */
  fontInterface: "'Noto Sans'",
  fontHeading: "'Noto Sans'",
  fontReading: "'Noto Serif'",
  fontWriting: "'Ubuntu mono'",
  fontSizeBase: '18px',
  fontSizeBaseSmall: '16px',
  fontSizeHeading1: '36px',
  fontSizeHeading2: '32px',
  fontSizeHeading3: '29px',
  fontSizeHeading4: '26px',
  fontSizeHeading5: '23px',
  fontSizeHeading6: '20px',
  fontLineHeight: '32px',

  /* Spacing */
  gridUnit: '32px',
  subGridUnit: '8px',

  /* Border */
  borderRadius: '8px',
  borderWidth: '1px',
  borderStyle: 'solid',

  /* Shadow (for tooltip) */
  boxShadow: '0 2px 4px 0 rgba(51, 51, 51, 0.3)',

  /* Transition */
  transitionDuration: '1s',
  transitionDurationM: '0.5s',
  transitionDurationS: '0.2s',
  transitionDurationXs: '0.1s',
  transitionTimingFunction: 'ease',
  transitionDelay: '500ms',
}
